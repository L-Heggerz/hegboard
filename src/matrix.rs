use std::sync::Mutex;

use crate::webserver::Pixels;

#[cfg(feature = "prod")]
use rpi_led_matrix::{LedColor, LedMatrix, LedMatrixOptions, LedRuntimeOptions};

#[cfg(feature = "prod")]
use std::time::SystemTime;

#[cfg(not(feature = "prod"))]
use std::{
    fs::{remove_file, File},
    io::Write,
};

pub trait Matrix {
    fn draw_pixels(&self, pixels: &Pixels) -> Result<(), std::io::Error>;
}

#[cfg(feature = "prod")]
struct RealMatrix {
    matrix: Mutex<LedMatrix>,
}

#[cfg(feature = "prod")]
pub fn new() -> impl Matrix {
    log::info!("Initialising RPiMatrix");

    let mut rtOptions = LedRuntimeOptions::new();
    rtOptions.set_gpio_slowdown(2);

    let mut options = LedMatrixOptions::new();
    options.set_hardware_mapping("adafruit-hat-pwm");
    options.set_rows(64);
    options.set_cols(64);
    options.set_chain_length(2);
    options.set_hardware_pulsing(true);
    options.set_pwm_dither_bits(1);
    RealMatrix {
        matrix: Mutex::new(
            LedMatrix::new(Some(options), Some(rtOptions)).expect("Could not initialise matrix!"),
        ),
    }
}

#[cfg(feature = "prod")]
impl Matrix for RealMatrix {
    fn draw_pixels(&self, pixels: &Pixels) -> Result<(), std::io::Error> {
        let lock_start = SystemTime::now();
        let matrix = self.matrix.try_lock().expect("failed to get lock");
        log::debug!("lock time: {:?}", lock_start.elapsed().unwrap());
        let canvas_start = SystemTime::now();
        let mut canvas = matrix.offscreen_canvas();
        log::debug!("canvas init time: {:?}", canvas_start.elapsed().unwrap());
        let populate_start = SystemTime::now();
        for x in 0..128 {
            for y in 0..64 {
                let color = &pixels.pixels[x][y];
                canvas.set(
                    x as i32,
                    y as i32,
                    &LedColor {
                        red: color.r,
                        green: color.g,
                        blue: color.b,
                    },
                );
            }
        }
        log::debug!(
            "canvas population time: {:?}",
            populate_start.elapsed().unwrap()
        );
        let swap_start = SystemTime::now();
        matrix.swap(canvas);
        log::debug!("canvas swap time: {:?}", swap_start.elapsed().unwrap());
        Result::Ok(())
    }
}

#[cfg(feature = "prod")]
unsafe impl Sync for RealMatrix {}

#[cfg(feature = "prod")]
unsafe impl Send for RealMatrix {}

//
//  MATRIX FOR TESTING
//

#[cfg(not(feature = "prod"))]
struct FakeLoggingMatrix {
    logfile: Mutex<File>,
    path: String,
}

#[cfg(not(feature = "prod"))]
pub fn new() -> impl Matrix {
    use std::{env, ffi::OsString};

    log::info!("Initialising FakeLoggingMatrix");

    let filename = env::var_os("HEGBOARD_LOGNAME")
        .unwrap_or(OsString::from("test-out"))
        .into_string()
        .unwrap();

    FakeLoggingMatrix {
        logfile: Mutex::new(
            File::create(&filename).expect("Could not create file for FakeLoggingMatrix"),
        ),
        path: filename,
    }
}

#[cfg(not(feature = "prod"))]
impl Matrix for FakeLoggingMatrix {
    fn draw_pixels(&self, pixels: &Pixels) -> Result<(), std::io::Error> {
        let buf = [
            "\n".as_bytes(),
            serde_json::to_string(&pixels.pixels)
                .expect("failed to reserialise json")
                .as_bytes(),
        ]
        .concat();
        self.logfile
            .lock()
            .expect("could not attain lock for log file")
            .write_all(&buf)
    }
}

#[cfg(not(feature = "prod"))]
impl Drop for FakeLoggingMatrix {
    fn drop(&mut self) {
        remove_file(&self.path).expect("Failed to cleanup logfile");
    }
}
