# HegBoard

HegBoard is a project designed to run on a Raspberry Pi connected to an RGB LED Matrix. It aims to be a dynamic, intelligent, visually appealing and almost entirely useless decoration.

## Commands

The code can be built with:

```
make build
```

Tests can be run with:

```
make test
```

You can compile locally for an ARM architecture with:

```
make docker
```
