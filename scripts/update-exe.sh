#!/bin/bash

set -e
curl -Lf --header "PRIVATE-TOKEN: $HEGBOARD_GITLAB_CI_ACCESS_TOKEN" "https://gitlab.com/api/v4/projects/30595732/jobs/artifacts/main/download?job=release-job" --output hegboard.zip
unzip hegboard.zip
rm hegboard.zip