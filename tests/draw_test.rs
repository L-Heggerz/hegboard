mod server;

use std::fs;

use actix_rt;
use reqwest;
use server::HegBoardServer;

#[derive(Debug)]
#[allow(dead_code)]
struct Pixel {
    r: u8,
    g: u8,
    b: u8,
}

#[actix_rt::test]
async fn returns_500_for_invalid_data_size() {
    let hegboard: HegBoardServer = HegBoardServer::new().await;
    let client = reqwest::Client::new();
    let result = client
        .post(hegboard.url.to_owned() + "/app/draw")
        .body("[[{r: 1, g: 1, b: 1}]]")
        .send()
        .await
        .expect("No Result From Version Endpoint");
    assert_eq!(result.status(), reqwest::StatusCode::BAD_REQUEST);
}

#[actix_rt::test]
async fn returns_200_for_correct_data_size_and_logs_data() {
    let hegboard: HegBoardServer = HegBoardServer::new().await;
    let client = reqwest::Client::new();
    let json = generate_pixels(4, 2, 0);
    println!("json: {}", json);
    let result = client
        .post(hegboard.url.to_owned() + "/app/draw")
        .header("Content-Type", "application/json")
        .body(format!("{{ \"pixels\": {} }}", json))
        .send()
        .await
        .expect("No Result From Version Endpoint");
    assert_eq!(result.status(), reqwest::StatusCode::OK);
    assert_eq!(
        fs::read_to_string(&hegboard.path).unwrap().contains(&json),
        true
    );
}

fn generate_pixels(r: u8, g: u8, b: u8) -> String {
    let mut pixels = Vec::new();
    for _x in 0..128 {
        let mut col: Vec<Pixel> = Vec::new();
        for _y in 0..64 {
            col.push(Pixel { r, g, b })
        }
        pixels.push(col);
    }
    println!("{}", pixels.len());
    format!("{:?}", pixels)
        .replace("Pixel ", "")
        .replace("r", "\"r\"")
        .replace("g", "\"g\"")
        .replace("b", "\"b\"")
        .replace(" ", "")
}
